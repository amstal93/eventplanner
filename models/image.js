module.exports = (sequelize, Sequelize) => sequelize.define('image', {
  path: {
    type: Sequelize.STRING,
  },
});
