const path = require('path');
const dotenv = require('dotenv');

if (process.env && process.env.NODE_ENV) {
  dotenv.config({ path: `.env.${process.env.NODE_ENV}` });
} else {
  dotenv.config({ path: '.env.development' });
}

const config = {
  app: {
    port: process.env.PORT || 8080,
    staticDir: path.join(__dirname, 'static'),
    uploadDir: path.join(__dirname, 'user_images'),
  },
  db: {
    url: process.env.DB_URL || process.env.DATABASE_URL,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  },
  handlebars: {
    views: path.join(__dirname, 'views'),
    extname: 'hbs',
    defaultView: 'main',
    layoutsDir: path.join(__dirname, 'views/layouts'),
    partialsDir: path.join(__dirname, 'views/partials'),
  },
  session: {
    secret: '142e6ecf42884f03',
  },
  hashing: {
    hashSize: 32,
    saltSize: 16,
    algorithm: 'sha512',
    iterations: 1000,
  },
};

module.exports = config;
