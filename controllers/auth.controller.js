const { validationResult } = require('express-validator');
const crypto = require('crypto');
const { logErrors } = require('../utils/validator_utils');
const { User } = require('../db/db');
const config = require('../config');

function showLoginForm(req, res) {
  res.render('auth/login', { errors: req.flash('error') });
}

function showRegistrationForm(req, res) {
  res.render('auth/register', { errors: req.flash('error') });
}

function register(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    logErrors(errors);
    req.flash('error', errors.array());
    res.status(401).redirect('/register');
    return;
  }
  const {
    saltSize, iterations, hashSize, algorithm,
  } = config.hashing;
  crypto.randomBytes(saltSize, (err, salt) => {
    if (err) {
      console.error(`Salt generation unsuccessful: ${err.message}`);
      res.status(500).render('error', { message: 'Internal Server Error: Salt generation unsuccessful' });
      return;
    }

    const { password } = req.body;
    crypto.pbkdf2(password, salt, iterations, hashSize, algorithm, (hashErr, hash) => {
      if (hashErr) {
        console.error(`Hashing unsuccessful: ${hashErr.message}`);
        res.status(500).render('error', { message: 'Internal Server Error: Hashing unsuccessful' });
        return;
      }

      const hashWithSalt = Buffer.concat([hash, salt]).toString('hex');

      User.create({
        username: req.body.username,
        fullName: req.body.fullName,
        password: hashWithSalt,
        role: 'user',
      })
        .then((user) => user.get())
        .then((user) => {
          req.session.userId = user.id;
          req.session.username = req.body.username;
          req.session.role = 'user';
          res.redirect('/');
        });
    });
  });
}

function login(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    logErrors(errors);
    req.flash('error', errors.array());
    res.status(401).redirect('/login');
    return;
  }

  const { username, password } = req.body;
  const { iterations, hashSize, algorithm } = config.hashing;

  User.findOne({ where: { username } })
    .then((user) => user.get())
    .then((user) => {
      const expectedHash = user.password.substring(0, hashSize * 2);
      const salt = Buffer.from(user.password.substring(hashSize * 2), 'hex');

      crypto.pbkdf2(password, salt, iterations, hashSize, algorithm, (err, binaryHash) => {
        if (err) {
          console.error(`Hashing unsuccessful: ${err.message}`);
          res.status(500).render('error', { message: 'Internal Server Error: Hashing unsuccessful' });
          return;
        }

        const actualHash = binaryHash.toString('hex');
        if (expectedHash !== actualHash) {
          req.flash('error', [{ msg: 'Hibás jelszó!' }]);
          res.status(401).redirect('/login');
          return;
        }

        req.session.userId = user.id;
        req.session.username = username;
        req.session.role = user.role;
        res.redirect('/');
      });
    });
}

function logout(req, res) {
  req.session.destroy((err) => {
    if (err) {
      console.error(`Session destroy failed: ${err.msg}`);
      res.render('error', { message: 'Internal Server Error: Session destroy failed' });
      return;
    }

    res.redirect('/');
  });
}

module.exports = {
  showLoginForm,
  showRegistrationForm,
  login,
  logout,
  register,
};
