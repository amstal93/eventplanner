const { validationResult } = require('express-validator');
const fs = require('fs');
const path = require('path');
const { Image } = require('../db/db');
const { logErrors } = require('../utils/validator_utils');

function store(req, res) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    logErrors(errors);
    fs.unlink(req.files.image.path, () => {});

    req.flash('error', errors.array());
    res.status(422).redirect(`/event/${req.fields.eventId}`);
    return;
  }

  const { eventId, userId } = req.fields;
  const filePath = req.files.image.path;
  const extension = path.extname(req.files.image.name);
  const newPath = filePath + extension;
  fs.rename(filePath, newPath, () => {});

  Image.create({
    path: path.join('/', path.basename(newPath)),
    eventId,
    userId,
  }).then(() => {
    console.log(`Image ${path.basename(newPath)} added to event ${eventId}`);
    res.redirect(`/event/${eventId}`);
  });
}

module.exports = {
  store,
};
